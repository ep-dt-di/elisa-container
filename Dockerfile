FROM gitlab-registry.cern.ch/linuxsupport/alma9-base

EXPOSE 8080

RUN dnf install -y 'dnf-command(config-manager)' && \
    dnf config-manager --add-repo https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/yum/tdaq/elisa/el9/ && \
    dnf install -y java-11-openjdk elisa httpd mod_auth_openidc --nogpgcheck && \
    dnf erase -y 'dnf-command(config-manager)' && dnf clean all

WORKDIR /var/local/elisa
COPY ./config ./config

RUN chgrp -R 0 /var/local/elisa && \
    chmod -R g=u /var/local/elisa

USER 1001
