# Containerized deployment of the ELisA electronic logbook application

The ELisA logbook is developed and maintained by the ATLAS TDAQ Controls & Configuration group. 

Firstly, get in contact with the developer at this [link](https://twiki.cern.ch/twiki/bin/viewauth/Sandbox/ELisAInstallation) if you are interested in using ELisA.

Once the initial database configuration is complete, you can follow the rest of this README to set up a deployment on OKD4 PaaS.

## Deployment

The application can be installed on [OKD4 PaaS](https://paas.docs.cern.ch) by instantiating the [OpenShift Template](./openshift-template.yaml) that comes with this repository.
Given a few configuration parameters, it will create all the necessary resources in the PaaS project.

```sh
# go to LXPLUS
ssh <USER>@lxplus.cern.ch

# log in to OKD PaaS
oc sso-login --server https://api.paas.okd.cern.ch

# create a new project (if necessary) - please choose a DESCRIPTIVE name and description
oc create project <PROJECT_NAME> --description "ELiSA logbook application for <DEPARTMENT/EXPERIMENT/PURPOSE>"

# show all parameters that need to be set:
oc process --parameters -f https://gitlab.cern.ch/ep-dt-di/elisa-container/-/raw/master/openshift-template.yaml

# set parameters and create resources:
oc process -f https://gitlab.cern.ch/ep-dt-di/elisa-container/-/raw/master/openshift-template.yaml \
    -p APP_HOSTNAME=example-elisa.app.cern.ch \
    ... \
    | oc create -f -
```

Alternatively, the OKD Web Console can be used:
* Follow [Creating a PaaS project](https://paas.docs.cern.ch/1._Getting_Started/1-create-paas-project/)
* Click on the `+` (plus) button in the top-right corner and copy-paste the contents from `openshift-template.yaml` (this needs to be done only once per project).
* Go to the `+Add` tab in the sidebar on the left, click on `Developer Catalog` > `All Services` and search for *"elisa"*
* Click on the card and then on the blue `Instatiate template` button.
* Provide the required configuration parameters and click on `Create`.

### Updates

By default, the deployed application is automatically updated (within ~15 minutes) whenever a new container image is available in the [Gitlab Container Registry](https://gitlab.cern.ch/ep-dt-di/elisa-container/container_registry).
To disable this behavior, the `ImageStream` option `spec.tags.importPolicy.scheduled` needs to be set to `false` (this can be done after the initial deployment of the template).

To manually start a new import for a particular imagestream, use the following commands:

```
oc get imagestreams
oc import-image <IMAGESTREAM_NAME>:latest
```

### Cleanup

To delete the resources, we can either delete the entire OKD PaaS project or delete all resources associated to the template:

```
LOGBOOK_NAME=example # the name used when instantiating the template
oc delete imagestream,service,route,deployment,configmap,secret,oidcreturnuri -l app.kubernetes.io/instance=$LOGBOOK_NAME --dry-run=server
```

Remove the `--dry-run` flag to perform the operation.

## Authentication

Authentication (identifying users) is handled by an Apache httpd server with [mod_auth_openidc](https://github.com/OpenIDC/mod_auth_openidc/wiki).
It forwards authenticated requests to the ELisA Java application with the required HTTP headers.

Authorization (granting permissions to users) is handled by the Single-Sign-On and can be configured in the [Application Portal](https://application-portal.web.cern.ch/) - see details in ["Configuring authorized users"](https://paas.docs.cern.ch/4._CERN_Authentication/3-configuring-authorized-users/).
